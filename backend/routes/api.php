<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Api\PostsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Orion\Facades\Orion;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// PUBLIC ROUTES
Route::post('/spa/register', [AuthController::class, 'register']);
Route::post('/spa/login', [AuthController::class, 'login']);

// PROTECTED ROUTES
Route::group(['middleware' => ['auth:sanctum']], function() {

    Route::post('/spa/logout', [AuthController::class, 'logout']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['as' => 'api.'], function() {
    Orion::resource('posts', PostsController::class);
});