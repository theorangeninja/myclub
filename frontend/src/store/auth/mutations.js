export const setIsAuthenticated = (state) => {
  state.isAuthenticated = true
}
export const removeIsAuthenticated = (state) => {
  state.isAuthenticated = false
}
