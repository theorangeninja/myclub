import { api } from 'src/boot/axios'

export const doLogin = async ({ commit }, payload) => {
  await api.post('/api/spa/login', payload)
    .then(response => {
      commit('setIsAuthenticated')
    })
}
export const doLogout = async ({ commit }) => {
  await api.post('/api/spa/logout')
    .then(response => {
      commit('removeIsAuthenticated')
    })
}
